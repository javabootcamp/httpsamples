package org.bootcamp.java.server;

import org.bootcamp.java.server.model.Employee;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class Cache {

    private Map<Integer, Employee> cache;

    public Cache(){
        cache = new HashMap<>();

        cache.put(1,new Employee(1,"John","Doe","CEO",26));
        cache.put(2,new Employee(2,"Albert","Eagle","CTO",36));
        cache.put(3,new Employee(3,"Jane","Bird","CFO",31));

    }

    public void add(Employee e){
        if(!cache.containsKey(e.getId())) {
            cache.put(e.getId(), e);
        } else {
            throw new IllegalArgumentException("ID taken");
        }
    }

    public Employee get(Integer id){
        return cache.get(id);
    }

    public Collection<Employee> getAll(){
        return cache.values();
    }

}
