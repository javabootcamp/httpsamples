package org.bootcamp.java.server;


import org.bootcamp.java.server.model.Employee;
import org.bootcamp.java.server.model.StatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Controller
public class ControllerServer {

    @Autowired
    private Cache cache;

    @RequestMapping(value = "/employees", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public Collection<Employee> getAllEmployees() {
        return cache.getAll();
    }

    @RequestMapping(value = "/employee", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public Employee getEmployee(@RequestParam(name = "id") Integer id) {
        return cache.get(id);
    }

    @RequestMapping(value = "/employee", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseBody
    public StatusResponse addNewEmployee(@RequestBody Employee employee) {
        try {
            cache.add(employee);
            return new StatusResponse(0, "OK");
        } catch (IllegalArgumentException e) {
            return new StatusResponse(-1, e.getMessage());
        }
    }

}
