package org.bootcamp.java.examples.apache;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bootcamp.java.server.model.Employee;

public class HttpClientExample {

    public static void main(String[] args) throws Exception {

        HttpClientExample http = new HttpClientExample();

        http.sendGet();

        http.sendPost();

    }

    // HTTP GET request
    private void sendGet() throws Exception {
        System.out.println("Starting Test - Send HTTP GET Request");

        String url = "http://localhost:8080/employees";

        //Create Apache Http Client, object that will be handling requests
        HttpClient client = HttpClientBuilder.create().build();
        //Create GET request object
        HttpGet request = new HttpGet(url);
        //Execute get request via client, save response in response object
        HttpResponse response = client.execute(request);

        //Craete reader, to process response content which is of type InputStream
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        //Iterate over the lines in response and append them to StringBuilder
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            builder.append(line);
        }

        //Get the output in a single string
        String result = builder.toString();

        //Use ObjectMapper to transform to Java object if result string is JSON

        System.out.println(result);
        System.out.println("Finished Test - Send HTTP GET Request");


    }

    // HTTP POST request
    private void sendPost() throws Exception {

        System.out.println("Starting Test - Send HTTP POST Request");

        String url = "http://localhost:8080/employee";

        //Create Apache Http Client, object that will be handling requests
        HttpClient client = HttpClientBuilder.create().build();
        //Create POST request object
        HttpPost post = new HttpPost(url);

        // Add header to POST request
        post.setHeader("Content-Type", "application/json");
        //Snippet how to set authorization header if necessary
        //post.setHeader("Authorization", "Bearer " + token);

        //Create object to send in POST body
        Employee employee = new Employee(999, "Test", "Tester", "Testing things", 87);
        ObjectMapper objectMapper = new ObjectMapper();
        //Transform employee object into JSON string and set as request body
        post.setEntity(new StringEntity(objectMapper.writeValueAsString(employee)));

        //Execute get request via client, save response in response object
        HttpResponse response = client.execute(post);

        //Craete reader, to process response content which is of type InputStream
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        //Iterate over the lines in response and append them to StringBuilder
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            builder.append(line);
        }

        //Get the output in a single string
        String result = builder.toString();

        //Print out the result
        System.out.println("Finished Test - Send HTTP POST Request");


    }

}
