package org.bootcamp.java.examples.spring;

import org.bootcamp.java.server.model.Employee;
import org.bootcamp.java.server.model.StatusResponse;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

public class RestTemplateExample {

    public static void main(String[] args) {

        RestTemplateExample example = new RestTemplateExample();

        example.doGetSingle();

        example.doGetCollection();

        example.doPost();

    }

    /*
        Method which demonstrates how to execute HTTP POST request with custom body and headers

        When restTemplate.postForObject is executed employee object is automatically converted
        into JSON String before executing the request.
        After request response JSON is automatically converted into StatusResponse object
     */
    public void doPost(){
        System.out.println("Starting Test - Send HTTP POST Request");

        //Create RestTemplate - class that handles all http requests
        RestTemplate restTemplate = new RestTemplate();

        //Create data object to send as POST body
        Employee employee = new Employee(888, "Testy", "Testery", "Testing things", 87);


        //Set Headers for request
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //Use BearerAuth header for OAuth token
        //headers.setBearerAuth();

        //Create HttpEntity object from request data and headers
        HttpEntity<Employee> request = new HttpEntity<>(employee, headers);

        //Execute POST call, and get the response back into statusResponse object
        StatusResponse statusResponse =
                restTemplate.postForObject("http://localhost:8080/employee",request,StatusResponse.class);


        //Print response body
        System.out.println(statusResponse);
        System.out.println("Finished Test - Send HTTP POST Request");
    }

    /*
        Method which demonstrates how to execute HTTP GET request with single result
     */
    public void doGetSingle(){

        System.out.println("Starting Test - Send HTTP GET Single Request");


        //Create RestTemplate - class that handles all http requests
        RestTemplate restTemplate = new RestTemplate();

        //Execute GET request store result in entity object
        ResponseEntity<Employee> entity = restTemplate.getForEntity("http://localhost:8080/employee?id=1",Employee.class);

        //Get body out of the result
        Employee employee = entity.getBody();

        //entity.getStatusCode() can be used to get the response code - 200 - OK

        //Print the result
        System.out.println(employee);
        System.out.println("Finished Test - Send HTTP GET Single Request");
    }

    /*
        Method which demonstrates how to execute HTTP GET request with list as result
     */
    public void doGetCollection() {

        System.out.println("Starting Test - Send HTTP GET Collection Request");

        //Create RestTemplate - class that handles all http requests
        RestTemplate restTemplate = new RestTemplate();


        //Execute GET request store result in entity object
        ResponseEntity<Collection<Employee>> rateResponse =
                restTemplate.exchange("http://localhost:8080/employees",
                        HttpMethod.GET, null, new ParameterizedTypeReference<Collection<Employee>>() {
                        });

        //Get body out of the result
        Collection<Employee> listOfBodies = rateResponse.getBody();

        //Print the result
        System.out.println(listOfBodies);
        System.out.println("Finished Test - Send HTTP GET Collection Request");


    }
}
